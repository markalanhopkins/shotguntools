// Fill out your copyright notice in the Description page of Project Settings.

#include "ShotgunTools.h"
#include<memory>
#include<string>
using namespace std;



ShotgunAsset::ShotgunAsset(string Name, size_t ID) : ShotgunEntity(Name, ID) {}


const std::vector<std::shared_ptr<ShotgunTask>>& ShotgunAsset::GetTasks() const
{
	return Tasks;
}


void ShotgunAsset::AddTask(std::shared_ptr<ShotgunTask> Task)
{
	Tasks.push_back(Task);
}


const std::vector<std::shared_ptr<ShotgunUser>>& ShotgunTask::GetAssignees() const
{
	return Assignees;
}


const std::shared_ptr<ShotgunAsset>& ShotgunTask::GetAsset() const
{
	return Asset;
}


std::string ShotgunTask::GetPipelineStep() const
{
	return PipelineStep;
}


void ShotgunTask::SetPipelineStep(std::string Step)
{
	PipelineStep = Step;
}


void ShotgunTask::AddAssignee(std::shared_ptr<ShotgunUser> NewAssignee)
{
	Assignees.push_back(NewAssignee);
}


void ShotgunTask::SetAsset(std::shared_ptr<ShotgunAsset> NewAsset)
{
	Asset = NewAsset;
}


size_t ShotgunEntity::GetID() const
{
	return ID;
}


std::string ShotgunEntity::GetName() const
{
	return Name;
}



template <class TShotgunEntitySubtype>
shared_ptr<TShotgunEntitySubtype> ShotgunRepo::Find(size_t ID)  
{
	auto Iterator = ShotgunEntities.find(ID);
	if (ShotgunEntities.end() == Iterator) 
	{
		return nullptr;
	}
	else 
	{
		// Downcast on retrieval from base-type storage
		return static_pointer_cast<TShotgunEntitySubtype>(Iterator->second);
	}
}



template <class TShotgunEntitySubtype>
shared_ptr<TShotgunEntitySubtype> ShotgunRepo::FindOrAdd(string Name, size_t ID) 
{
	auto Iterator = ShotgunEntities.find(ID);
	if (ShotgunEntities.end() == Iterator) 
	{
		++ObjectsCreated; // Debug

		// The Entity doesn't exist in the Repo, create one:
		shared_ptr<TShotgunEntitySubtype> NewEntity = make_shared<TShotgunEntitySubtype>(Name, ID);

		// Store by subtype
		StoreByType(NewEntity); 
		// Store by base type
		ShotgunEntities[ID] = NewEntity; 

		return NewEntity;
	}
	else 
	{ 
		shared_ptr<ShotgunEntity> Retrieved = Iterator->second;
		// Downcast on retrieval from base type storage. Template takes care of correct type so we static_cast.
		shared_ptr<TShotgunEntitySubtype> CorrectSubtype = static_pointer_cast<TShotgunEntitySubtype>(Retrieved);
		return  CorrectSubtype;
	}

}





void ShotgunRepo::StoreByType(std::shared_ptr<ShotgunEntity> Entity)
{
	// Downcast to each subtype and check for success.

	shared_ptr<ShotgunAsset> NewAsset = dynamic_pointer_cast<ShotgunAsset>(Entity);
	if (NewAsset)
	{
		// cout << "Adding " << NewAsset->GetName() << " to subtype containers \n"; // Debug
		ShotgunAssets[NewAsset->GetID()] = NewAsset;
		return;
	}

	shared_ptr<ShotgunTask> NewTask = dynamic_pointer_cast<ShotgunTask>(Entity);
	if (NewTask)
	{
		// cout << "Adding " << NewTask->GetName() << " to subtype containers \n"; // Debug
		ShotgunTasks.push_back(NewTask);
		return;
	}

	shared_ptr<ShotgunUser> NewUser = dynamic_pointer_cast<ShotgunUser>(Entity);
	if (NewUser)
	{
		// cout << "Adding " << NewUser->GetName() << " to subtype containers \n"; // Debug
		ShotgunUsers.push_back(NewUser);
		return;
	}

	shared_ptr<ShotgunTaskTemplate> NewTaskTemplate = dynamic_pointer_cast<ShotgunTaskTemplate>(Entity);
	if (NewTaskTemplate)
	{
		// cout << "Adding " << NewTaskTemplate->GetName() << "to subtype containers \n"; // Debug
		ShotgunTaskTemplates.push_back(NewTaskTemplate);
		return;
	}

}

bool ShotgunRepo::IsInRepo(size_t ID) const
{
	return ShotgunEntities.find(ID) != ShotgunEntities.end();
}


const std::vector<std::shared_ptr<ShotgunUser>>& ShotgunRepo::GetShotgunUsers() const
{
	return ShotgunUsers;
}


const std::vector<std::shared_ptr<ShotgunTaskTemplate>>& ShotgunRepo::GetShotgunTaskTemplates() const
{
	return ShotgunTaskTemplates;
}




// Static debug var init
int ShotgunRepo::ObjectsCreated = 0;




bool ShotgunConnection::IsInRepo(size_t ID) const
{
	return (Repo.IsInRepo(ID));
}



void ShotgunConnection::LoadAllUsersForProject(size_t ProjectID)
{
	// Use the Python helper functions to live-load from the Shotgun DB
	Py_Initialize();
	PyObject *PyModuleName, *PyModule, *PyFunc;
	PyObject *PyArgs, *PyTemp;

	// Python module distributed to site packages by pip
	const char* ModuleName = "lyquidShotgun.shotgunUtils";
	const char* FunctionName = "GetAllUsersForProject";
	PyModuleName = PyString_FromString(ModuleName);
	PyModule = PyImport_Import(PyModuleName);
	Py_DECREF(PyModuleName);

	if (PyModule != NULL) 
	{
		PyFunc = PyObject_GetAttrString(PyModule, FunctionName);
		// PyFunc is a new reference 
		if (PyFunc && PyCallable_Check(PyFunc)) 
		{
			PyArgs = PyTuple_New(1);
			PyTemp = PyInt_FromLong(ProjectID);
			// PyTemp reference stolen here: 
			PyTuple_SetItem(PyArgs, 0, PyTemp);
			PyTemp = PyObject_CallObject(PyFunc, PyArgs);

			if (PyTemp != NULL) 
			{
				Py_ssize_t Length = PyList_Size(PyTemp);
				for (int i = 0; i < Length; ++i) 
				{
					std::string  UserName = PyString_AsString(PyTuple_GetItem(PyList_GetItem(PyTemp, i), 0));
					size_t ID = PyInt_AsLong(PyTuple_GetItem(PyList_GetItem(PyTemp, i), 1));
					// Add to C++ model
					shared_ptr<ShotgunUser> NewUser = Repo.FindOrAdd<ShotgunUser>(UserName, ID);
					cout << "Creating: " << NewUser->GetName() << endl << endl;

				}
			}
		}
		Py_XDECREF(PyFunc);
		Py_DECREF(PyModule);
	}
	Py_Finalize();
}




void ShotgunConnection::LoadAllTaskTemplatesForProject(size_t ProjectID)
{
	Py_Initialize();
	PyObject *PyFunc;
	PyObject *PyArgs, *PyProjectID, *PythonListOfTaskTemplates;
	const char* FunctionName = "GetAllTaskTemplatesForProject";
	string ModuleName = "lyquidShotgun.shotgunUtils";
	PyObject* PyModuleName = PyString_FromString(ModuleName.c_str());
	PyObject* PyModule = PyImport_Import(PyModuleName);
	Py_DECREF(PyModuleName);
	PyFunc = PyObject_GetAttrString(PyModule, FunctionName);

	if (PyFunc && PyCallable_Check(PyFunc)) 
	{
		PyArgs = PyTuple_New(1);
		PyProjectID = PyInt_FromLong(ProjectID);
		PyTuple_SetItem(PyArgs, 0, PyProjectID);
		PythonListOfTaskTemplates = PyObject_CallObject(PyFunc, PyArgs);

		if (PythonListOfTaskTemplates != NULL) 
		{
			Py_ssize_t PyTaskTemplateListLength = PyList_Size(PythonListOfTaskTemplates);
			PyObject* PyTaskTemplateDict;
			for (int CurTaskTemplateIndex = 0; CurTaskTemplateIndex < PyTaskTemplateListLength; ++CurTaskTemplateIndex) 
			{
				PyTaskTemplateDict = PyList_GetItem(PythonListOfTaskTemplates, CurTaskTemplateIndex);
				if (PyTaskTemplateDict) 
				{
					// Less null-checking here as we know that all entities have code and id
					int TaskTemplateID = PyInt_AsLong(PyDict_GetItem(PyTaskTemplateDict, PyString_FromString("id")));
					string TaskTemplateName = PyString_AsString(PyDict_GetItem(PyTaskTemplateDict, PyString_FromString("code")));
					//Add to C++ model
					Repo.FindOrAdd<ShotgunTaskTemplate>(TaskTemplateName, TaskTemplateID);
				}
			}
		}
		Py_XDECREF(PyFunc);
		Py_DECREF(PyModule);
	}
	Py_Finalize();
}



int ShotgunConnection::CreateAssetWithTaskTemplate(string AssetName, size_t TaskTemplateID) 
{
	Py_Initialize();
	PyObject* PyFunc;
	PyObject* PyFuncArgs;
	PyObject* PyTemp;
	const char* FunctionName = "CreateAssetWithTaskTemplate";
	int ConfirmedAssetID;
	string ModuleName = "lyquidShotgun.shotgunUtils";

	PyObject* PyModuleName = PyString_FromString(ModuleName.c_str());
	PyObject* PyModule = PyImport_Import(PyModuleName);
	Py_DECREF(PyModuleName);
	PyFunc = PyObject_GetAttrString(PyModule, FunctionName);

	if (PyFunc && PyCallable_Check(PyFunc)) 
	{

		PyFuncArgs = PyTuple_New(2);
		PyTemp = PyString_FromString(AssetName.c_str());
		PyTuple_SetItem(PyFuncArgs, 0, PyTemp);
		PyTemp = PyInt_FromLong(TaskTemplateID);
		PyTuple_SetItem(PyFuncArgs, 1, PyTemp);

		// Asset creation in Shotgun
		PyTemp = PyObject_CallObject(PyFunc, PyFuncArgs);
		ConfirmedAssetID = PyInt_AsLong(PyDict_GetItem(PyTemp, PyString_FromString("id")));

		// Update Repo with new asset, including live-load of all new tasks
		GetAllTasksForAsset(ConfirmedAssetID);
		

		Py_XDECREF(PyFunc);
		Py_DECREF(PyModule);
	}
	Py_Finalize();

	return ConfirmedAssetID;


}



const vector<shared_ptr<ShotgunTask>>& ShotgunConnection::GetAllTasksForAsset(size_t AssetID) 
{
	shared_ptr<ShotgunAsset> RepoAsset = Repo.Find<ShotgunAsset>(AssetID);
	// Case 1: Asset already exists in Repo
	if (RepoAsset) 
	{
		// Return by const ref to the vetor
		return RepoAsset->GetTasks();
	}

	// Case 2: Asset doesn't already exist: query Shotgun
	Py_Initialize();
	PyObject *PyFunc;
	PyObject *PyArgs, *PyAssetID, *PythonListOfTasks;
	const char* FunctionName = "GetAllTasksForAsset";
	string ModuleName = "lyquidShotgun.shotgunUtils";

	PyObject* PyModuleName = PyString_FromString(ModuleName.c_str());
	PyObject* PyModule = PyImport_Import(PyModuleName);
	Py_DECREF(PyModuleName);
	PyFunc = PyObject_GetAttrString(PyModule, FunctionName);

	if (PyFunc && PyCallable_Check(PyFunc)) 
	{
		PyArgs = PyTuple_New(1);
		PyAssetID = PyInt_FromLong(AssetID);
		PyTuple_SetItem(PyArgs, 0, PyAssetID);
		PythonListOfTasks = PyObject_CallObject(PyFunc, PyArgs);
		//Py_DECREF(pArgs);

		if (PythonListOfTasks != NULL) 
		{
			Py_ssize_t PyTaskListLength = PyList_Size(PythonListOfTasks);
			PyObject* PyPipelineStepDict;
			PyObject* PyAssigneesList;
			PyObject* PyAssigneeDict;
			PyObject* PyAssetDict;
			PyObject* StepName;

			for (int CurTaskIndex = 0; CurTaskIndex < PyTaskListLength; ++CurTaskIndex) 
			{
				PyObject* PyTaskDict = PyList_GetItem(PythonListOfTasks, CurTaskIndex);
				if (PyTaskDict) 
				{
					// TODO this nested call is brittle and will fail easily. Refactor with null checks and informative error-response
					int TaskID = PyInt_AsLong(PyDict_GetItem(PyTaskDict, PyString_FromString("id")));
					string TaskName = PyString_AsString(PyDict_GetItem(PyTaskDict, PyString_FromString("content")));

					// Create and store minimal Task in C++ model
					shared_ptr<ShotgunTask> Task = Repo.FindOrAdd<ShotgunTask>(TaskName, TaskID);

					// Now load other fields

					// Pipeline Step:
					PyPipelineStepDict = PyDict_GetItem(PyTaskDict, PyString_FromString("step"));
					if (PyPipelineStepDict) 
					{
						StepName = PyDict_GetItem(PyPipelineStepDict, PyString_FromString("name"));
						if (StepName) 
						{
							string PipelineStep = PyString_AsString(StepName);
							Task->SetPipelineStep(PipelineStep);
						}
					}

					// Asset:
					PyAssetDict = PyDict_GetItem(PyTaskDict, PyString_FromString("entity"));
					if (PyAssetDict) 
					{
						// In the case where the asset has just been created by this library, it will exist in Shotgun but not in the repo.
						// ExtractFromPyDict will FindOrAdd.
						// TODO refactor so this process is clearer to the calling code, currently too much like a load by side-effect
						shared_ptr<ShotgunAsset> Asset = ExtractFromPyDict<ShotgunAsset>(PyAssetDict);

						Task->SetAsset(Asset);
						// In Shotgun, Assets have no knowledge of Tasks assigned to them. In the C++ model we will keep a reference to speed up retrieval
						Asset->AddTask(Task);
					}

					// Assignees:
					PyAssigneesList = (PyDict_GetItem(PyTaskDict, PyString_FromString("task_assignees")));
					if (PyAssigneesList) 
					{
						Py_ssize_t PyAssigneesListLength = PyList_Size(PyAssigneesList);
						for (int CurAssigneeIndex = 0; CurAssigneeIndex < PyAssigneesListLength; ++CurAssigneeIndex) 
						{
							PyAssigneeDict = PyList_GetItem(PyAssigneesList, CurAssigneeIndex);
							if (PyAssigneeDict) 
							{
								shared_ptr<ShotgunUser> ExistingAssignee = ExtractFromPyDict<ShotgunUser>(PyAssigneeDict);
								Task->AddAssignee(ExistingAssignee); 

							}
						}

					}
									}
			}
			Py_XDECREF(PyFunc);
			Py_DECREF(PyModule);
		}
	}

	Py_Finalize();
	// TODO assumes valid asset exists which will not be the case if Python call failed. Refactor for nullcheck and fail safe with informative error-response
	
	vector<shared_ptr<ShotgunTask>> EmptyTasks;

	// This should have just been extracted from the live Shotgun call
	shared_ptr<ShotgunAsset> NewlyLoadedAsset = Repo.Find<ShotgunAsset>(AssetID);
	if (NewlyLoadedAsset)
	{
		// Return by const ref to repo tasks for this asset
		return NewlyLoadedAsset->GetTasks();
	}
	else
	{
		// The Python extraction failed
		return EmptyTasks;
		// TODO This needs to be more meaningful and throw different exceptions based on type of failure.
	}
	
}



void ShotgunConnection::AssignTaskToUser(size_t TaskID, size_t UserID) 
{
	Py_Initialize();
	PyObject* PyFunc;
	PyObject* PyFuncArgs;
	PyObject* PyTaskID;
	PyObject* PyUserID;
	const char* FunctionName = "AssignTaskToUser";
	string ModuleName = "lyquidShotgun.shotgunUtils";
	PyObject* PyModuleName = PyString_FromString(ModuleName.c_str());
	PyObject* PyModule = PyImport_Import(PyModuleName);
	Py_DECREF(PyModuleName);
	PyFunc = PyObject_GetAttrString(PyModule, FunctionName);

	if (PyFunc && PyCallable_Check(PyFunc)) 
	{
		PyFuncArgs = PyTuple_New(2);
		PyTaskID = PyInt_FromLong(TaskID);
		PyUserID = PyInt_FromLong(UserID);
		PyTuple_SetItem(PyFuncArgs, 0, PyTaskID);
		PyTuple_SetItem(PyFuncArgs, 1, PyUserID);

		// Assign in Shotgun. 	
		// TODO refactor so returns confirmation of assignment.
		PyObject_CallObject(PyFunc, PyFuncArgs);

		// Update Repo
		shared_ptr<ShotgunTask> RepoTask = Repo.Find<ShotgunTask>(TaskID);
		shared_ptr<ShotgunUser> RepoUser = Repo.Find<ShotgunUser>(UserID);
		if (RepoTask && RepoUser)
		{
			RepoTask->AddAssignee(RepoUser);
		}

		Py_XDECREF(PyFunc);
		Py_DECREF(PyModule);
	}

	Py_Finalize();




}



const std::vector<std::shared_ptr<ShotgunUser>>& ShotgunConnection::GetCachedUsers()
{
	return Repo.GetShotgunUsers();
}


const std::vector<std::shared_ptr<ShotgunTaskTemplate>>& ShotgunConnection::GetCachedTaskTemplates()
{
	return Repo.GetShotgunTaskTemplates();
}


template<class TShotgunEntitySubtype>
std::shared_ptr<TShotgunEntitySubtype> ShotgunConnection::ExtractFromPyDict(PyObject* PyDict) 
{
	size_t ID = PyInt_AsLong(PyDict_GetItem(PyDict, PyString_FromString("id")));
	std::string Name = PyString_AsString(PyDict_GetItem(PyDict, PyString_FromString("name")));
	return Repo.FindOrAdd<TShotgunEntitySubtype>(Name, ID);
}



// Placeholder for later work on sync issues with underling Shotgun DB
void ShotgunConnection::SetRepoNeedsRefresh(bool NeedsRefresh)
{
	RepoNeedsRefresh = NeedsRefresh;
}

