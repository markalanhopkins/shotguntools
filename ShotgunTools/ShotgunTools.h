
#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<memory>
#include<map>
#include<unordered_map>
#include<sstream>
#include<typeinfo>
#include<typeindex>
#include <Python.h>

/****************************************************************************************************************

Middle layer of the Unreal Shotgun plugin. Handles main interface between the Python API and C++. 
To be build as a static library included in Unreal Plugin.
Dependent on the pip-distributed lyquidShotgun.shotgunUtils Python helper which is the main interface with Shotgun.

Classes:
- ShotgunConnection: Main interface between Unreal and the Python api calls.
- Repo: Stores the partial model we build of the Shotgun database entities.

Notes:
- Every entity in Shotgun has a unique ID.
- The C++ ShotgunEntity is the base class storing ID and Name. Name is used chiefly for debug and map-key generation.
- Other Shotgun C++ classes derive from ShotgunEntity, some are only for code-clarity eg ShotgunTaskTemplate,
  others have member-vars and functions of their own, eg ShotgunTask.
- We will always create an Asset in Shotgun with a predefined TaskTemplate. This creates the asset and at the same time
  assigns a typical series of pipeline tasks to it. Eg create MoonLander also creates attached Model, Rig, and Animate tasks


*****************************************************************************************************************/


// Shotgun ID for test project
constexpr size_t GRAVITY_PROJECT_ID = 86;


// Base class for C++ representations of Shotgun entities 
class ShotgunEntity 
{
private:
	// Everything in Shotgun is tracked using this unique ID
	size_t ID;

	std::string Name;

protected:
	// Constructor
	ShotgunEntity(std::string Name, size_t ID) : Name(Name), ID(ID) {}

public:
	// Accessor
	size_t GetID() const;

	// Accessor
	std::string GetName() const;

	// Need one virtual function for downcasting on retrieval of base pointer from Repo
	virtual ~ShotgunEntity() = default;
};


// Forward declarations
class ShotgunUser;
class ShotgunAsset;

// Partial aggregation of Shotgun-schema task fields. Extend as needed.
// Main composite data structure we use in Unreal plugin. A task knows of its pipeline step, its asset and its assignees. 
class ShotgunTask final : public ShotgunEntity 
{
private:
	// Shotgun Users
	std::vector<std::shared_ptr<ShotgunUser>> Assignees;

	// Underlying asset this task refers to
	std::shared_ptr<ShotgunAsset> Asset;

	// Eg Animate, Rig, Model
	std::string  PipelineStep;

public:
	// Constructor
	ShotgunTask(std::string Name, size_t ID) : ShotgunEntity(Name, ID) {}

	/**
	* Sets the pipeline step for this task. 
	* @param PipelineStep - eg "Animate"
	*/
	void SetPipelineStep(std::string PipelineStep);

	/**
	* Adds a Shotgun user to this task.
	* @param Assignee - Shotgun user
	* 
	*/
	void AddAssignee(std::shared_ptr<ShotgunUser> Assignee);

	/**
	* Sets the underlying asset for this task.
	* @param Asset - Shotgun asset
	*/
	void SetAsset(std::shared_ptr<ShotgunAsset> Asset);

	// Const ref accessor
	const std::vector<std::shared_ptr<ShotgunUser>>& GetAssignees() const;

	// Const ref accessor
	const std::shared_ptr<ShotgunAsset>& GetAsset() const;

	// Accessor
	std::string GetPipelineStep() const;

};



/**
* Partial C++ aggregation of Shotgun-schema task template fields. Extend as needed.
* No specialization from ShotgunEntity here. Convenience subclass for code-readability. 
*/
class ShotgunTaskTemplate final : public ShotgunEntity 
{
public:
	// Constructor delegates directly to base class
	ShotgunTaskTemplate(std::string Name, size_t ID) : ShotgunEntity(Name, ID) {}
};




/**
* Partial C++ aggregation of Shotgun-schema asset fields. Extend as needed.
* The ID and Name fields will eventually be wrapped in a ShotgunData component attached to this asset's actor in the editor. 
*/
class ShotgunAsset final : public ShotgunEntity 
{
protected:
	/**
	* In Shotgun, assets have no knowledge of tasks assigned to them. In the C++ model we will keep 
	* a reference to assigned tasks to speed up retrieval for menu building.
	*/
	std::vector<std::shared_ptr<ShotgunTask>> Tasks;

public:
	// Constructor
	ShotgunAsset(std::string Name, size_t ID);

	// Const ref accessor
	const std::vector<std::shared_ptr<ShotgunTask>>& GetTasks() const;

	/**
	* Updates internal container of references to all the tasks connected with this asset.
	* @param Task - New task to track
	*/
	void AddTask(std::shared_ptr<ShotgunTask> Task);

};




//  Partial C++ aggregation of Shotgun-schema HumanUser fields. Extend as needed.
// No specialization from ShotgunEntity here. Convenience subclass for code-readability. 
class ShotgunUser final : public ShotgunEntity 
{
public:
	// Constructor delegates directly to base class
	ShotgunUser(std::string Name, size_t ID) : ShotgunEntity(Name, ID) {}
};



// Stores partial model of Shotgun database entities
class ShotgunRepo 
{
private:
	// Master map for all ShotgunEntities - accessed by unique ID
	std::unordered_map <size_t, std::shared_ptr<ShotgunEntity>> ShotgunEntities;

	// Debug var to check for duplication
	static int ObjectsCreated;

	// Convenience containers for ShotgunUsers. Typically loaded once on Unreal Plugin OnWorldCreated()
	std::vector<std::shared_ptr<ShotgunUser>> ShotgunUsers;

	// Convenience containers for TaskTemplates. Typically loaded once on Unreal Plugin OnWorldCreated()
	std::vector<std::shared_ptr<ShotgunTaskTemplate>> ShotgunTaskTemplates;

	// Not currently used, for later dev
	std::map<size_t, std::shared_ptr<ShotgunAsset>> ShotgunAssets;

	// Not currently used, for later dev
	std::vector <std::shared_ptr<ShotgunTask>> ShotgunTasks;


	// Stratifies entities also stored in the master ShotgunEntities map into convenience containers for faster retrival 
	void StoreByType(std::shared_ptr<ShotgunEntity>);


	
public:
	/**
	* Returns a previously-stored ShotgunEntity if it exists, nullptr if doesnt.
	* @param ID - Shotgun ID
	*/
	template<class TShotgunEntitySubtype>
	std::shared_ptr<TShotgunEntitySubtype> Find(size_t ID) ;


	/**
	* Returns a previously-stored ShotgunEntity if it exists, creates a new one if it doesn't. 
	* Avoids unnecessary duplicate shared_ptr creation as we read the same Asset ID while parsing a Shotgun call.
	* @param Name - used for debug and map-key generation
	* @param ID - Shotgun ID
	*/
	template<class TShotgunEntitySubtype>
	std::shared_ptr<TShotgunEntitySubtype> FindOrAdd(std::string Name, size_t ID );


	/**
	*Looks for any ShotgunEntity by ID in master map
	* @param ID - Shotgun ID
	*/
	bool IsInRepo(size_t ID) const;


	// Const ref accessor
	const std::vector<std::shared_ptr<ShotgunUser>>& GetShotgunUsers() const;


	// Const ref accessor
	const std::vector<std::shared_ptr<ShotgunTaskTemplate>>& GetShotgunTaskTemplates() const;

};



// Main interface between Unreal and the Python api calls
class ShotgunConnection 
{
private:
	// Master storage
	ShotgunRepo Repo;


	// For later work on sync and refresh with main Shotgun DB
	bool RepoNeedsRefresh = false;


	/**
	* Helper function for PyObject dictionary extraction.
	* @param PyUserDict - Python object to be parsed 
	*/
	template<class TShotgunEntitySubtype>
	std::shared_ptr<TShotgunEntitySubtype> ExtractFromPyDict(PyObject* PyDict);


	// Placeholder for later development on race conditions with underling Shotgun DB
	void SetRepoNeedsRefresh(bool NeedsRefresh);


public: 
	/**
	* Looks for any ShotgunEntity by ID in master map.
	* @param ID - Shotgun ID
	*/
	bool IsInRepo(size_t ID) const;


	/**
	* This data is unlikely to change per-session. Typically called once on Unreal plugin load.
	* @param ID - Shotgun ID
	*/
	void LoadAllUsersForProject(size_t ProjectID);


	/**
	* This data is unlikely to change per-session. Typically called once on Unreal plugin load.
	* @param ID - Shotgun ID
	*/
	void LoadAllTaskTemplatesForProject(size_t ProjectID);


	/**
	*  Loads the tasks previously created by the task template and any others that have been manually attached to this asset.
	*  First-time called there is a live-load from Shotgun, updating the Repo. Subsequent calls access the C++ model unless a refresh is needed.
	*/
	const std::vector<std::shared_ptr<ShotgunTask>>& GetAllTasksForAsset(size_t AssetID);


	//////////////////////////  Main Unreal-user plugin actions //////////////////////////

	/*  1: Creates Shotgun asset with the same name as the selected actor in Unreal.
	 *  2: Creates predefined series of Shotgun tasks linking to the asset.
	 *  3: Returns the unique ID of the Shotgun asset which is then stored in a newly created ShotgunData component in Unreal.
	 * @param AssetName - original editor-name
	 * @param TaskTemplateID - Used to run the task template creation in Shotgun
	 * @return Newly created Shotgun ID
	 */
	int CreateAssetWithTaskTemplate(std::string AssetName, size_t TaskTemplateID);


	/** 
	* Adds user to the Assignees field of a ShotgunTask, both in Shotgun and in C++ Repo model. 
	* @param TaskID - The Shotgun task to assign the new user to
	* @param UserID - The new assignee
	*/
	void AssignTaskToUser(size_t TaskID, size_t UserID);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	/**
	* Const ref accessor for the convenience containers - Used to generate the assign submenu in Unreal.
	* @return Task-assignable Shotgun users for this project 
	*/
	const std::vector<std::shared_ptr<ShotgunUser>>& GetCachedUsers();

	/**
	* Const ref accessor for the convenience containers - Used to generate the task template creation menu in Unreal.
	* @return Available task templates for this project
	*/
	const std::vector<std::shared_ptr<ShotgunTaskTemplate>>& GetCachedTaskTemplates(); 


};



