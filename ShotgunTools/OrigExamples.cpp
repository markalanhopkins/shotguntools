#include <Python.h>
#include<string>



void RawString() {
	Py_Initialize();
	PyRun_SimpleString("print 'Hola Mundo'\nprint 'Hola Womble'");
	Py_Finalize();

}

// From Official Python Docs
// https://docs.python.org/2/extending/embedding.html

int Basic(int argc, char *argv[])
{
	PyObject *pName, *pModule, *pDict, *pFunc;
	PyObject *pArgs, *pValue;
	int i;

	if (argc < 3) {
		fprintf(stderr, "Usage: call pythonfile funcname [args]\n");
		return 1;
	}

	Py_Initialize();
	pName = PyString_FromString(argv[1]);
	/* Error checking of pName left out */

	pModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (pModule != NULL) {
		pFunc = PyObject_GetAttrString(pModule, argv[2]);
		/* pFunc is a new reference */

		if (pFunc && PyCallable_Check(pFunc)) {
			pArgs = PyTuple_New(argc - 3);
			for (i = 0; i < argc - 3; ++i) {
				pValue = PyInt_FromLong(atoi(argv[i + 3]));
				if (!pValue) {
					Py_DECREF(pArgs);
					Py_DECREF(pModule);
					fprintf(stderr, "Cannot convert argument\n");
					return 1;
				}
				/* pValue reference stolen here: */
				PyTuple_SetItem(pArgs, i, pValue);
			}
			pValue = PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
			if (pValue != NULL) {
				printf("Result of call: %ld\n", PyInt_AsLong(pValue));
				Py_DECREF(pValue);
			}
			else {
				Py_DECREF(pFunc);
				Py_DECREF(pModule);
				PyErr_Print();
				fprintf(stderr, "Call failed\n");
				return 1;
			}
		}
		else {
			if (PyErr_Occurred())
				PyErr_Print();
			fprintf(stderr, "Cannot find function \"%s\"\n", argv[2]);
		}
		Py_XDECREF(pFunc);
		Py_DECREF(pModule);
	}
	else {
		PyErr_Print();
		fprintf(stderr, "Failed to load \"%s\"\n", argv[1]);
		return 1;
	}
	Py_Finalize();
	return 0;
}



// My First test
int  CheckModule1()

{
	PyObject *pName, *pModule, *pDict, *pFunc;
	PyObject *pArgs, *pArg1, *pArg2, *pResult;
	int i;
	const char* moduleName = "testModule";
	const char* FunctionName = "multiply";



	Py_Initialize();
	pName = PyString_FromString(moduleName);
	/* Error checking of pName left out */

	pModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (pModule != NULL) {
		pFunc = PyObject_GetAttrString(pModule, FunctionName);
		/* pFunc is a new reference */

		if (pFunc && PyCallable_Check(pFunc)) {
			pArgs = PyTuple_New(2);

			pArg1 = PyInt_FromLong(atoi("3"));
			if (!pArg1) {
				Py_DECREF(pArgs);
				Py_DECREF(pModule);
				printf("Cannot convert argument\n");
				return 1;
			}
			/* pArg1 reference stolen here: */
			PyTuple_SetItem(pArgs, 0, pArg1);

			pArg2 = PyInt_FromLong(atoi("15"));
			if (!pArg2) {
				Py_DECREF(pArgs);
				Py_DECREF(pModule);
				printf("Cannot convert argument\n");
				return 1;
			}
			/* pArg1 reference stolen here: */
			PyTuple_SetItem(pArgs, 1, pArg2);
			
			pResult = PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
			if (pResult != NULL) {
				printf("Result of call: %ld\n", PyInt_AsLong(pResult));
				Py_DECREF(pResult);
			}
			else {
				Py_DECREF(pFunc);
				Py_DECREF(pModule);
				PyErr_Print();
				printf("Call failed\n");
				return 1;
			}
		}
		else {
			if (PyErr_Occurred())
				PyErr_Print();
			printf("Cannot find function \"%s\"\n", FunctionName);
		}
		Py_XDECREF(pFunc);
		Py_DECREF(pModule);
	}
	else {
		PyErr_Print();
		printf("Failed to load \"%s\"\n", moduleName);
		return 1;
	}
	Py_Finalize();
}


// Simpler example
// http://realmike.org/blog/2012/07/05/supercharging-c-code-with-embedded-python/
std::string CallPythonPlugIn(const std::string& s)
{
	// Import the module "plugin" (from the file "plugin.py")
	PyObject* moduleName = PyString_FromString("plugin");
	PyObject* pluginModule = PyImport_Import(moduleName);
	// Retrieve the "transform()" function from the module.
	PyObject* transformFunc = PyObject_GetAttrString(pluginModule, "transform");
	// Build an argument tuple containing the string.
	PyObject* argsTuple = Py_BuildValue("(s)", s.c_str());
	// Invoke the function, passing the argument tuple.
	PyObject* resultList = PyObject_CallObject(transformFunc, argsTuple);
	// Convert the result to a std::string.
	std::string resultStr(PyString_AsString(resultList));
	// Free all temporary Python objects.
	Py_DECREF(moduleName); Py_DECREF(pluginModule); Py_DECREF(transformFunc);
	Py_DECREF(argsTuple); Py_DECREF(resultList);

	return resultStr;
}




int BasicMinusNullChecks(int argc, char *argv[])
{
	PyObject *pName, *pModule, *pFunc;
	PyObject *pArgs, *pValue;
	int i;



	Py_Initialize();
	pName = PyString_FromString(argv[1]);
	/* Error checking of pName left out */

	pModule = PyImport_Import(pName);
	Py_DECREF(pName);

	if (pModule != NULL) {
		pFunc = PyObject_GetAttrString(pModule, argv[2]);
		/* pFunc is a new reference */

		if (pFunc && PyCallable_Check(pFunc)) {
			pArgs = PyTuple_New(argc - 3);
			for (i = 0; i < argc - 3; ++i) {
				pValue = PyInt_FromLong(atoi(argv[i + 3]));
				/* pValue reference stolen here: */
				PyTuple_SetItem(pArgs, i, pValue);
			}
			pValue = PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
			if (pValue != NULL) {
				printf("Result of call: %ld\n", PyInt_AsLong(pValue));
				Py_DECREF(pValue);
			}

		}

		Py_XDECREF(pFunc);
		Py_DECREF(pModule);
	}

	Py_Finalize();
	return 0;
}



